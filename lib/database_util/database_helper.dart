import 'dart:io';

import 'package:flutter_components_demo_app/model/register_user.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final _databaseName = "MyDatabase.db";
  static final _databaseVersion = 1;

  static final tableName = "my_table";

  static final columnId = "_id";
  static final columnFirstName = "first_name";
  static final columnLastName = "last_name";

  DatabaseHelper._privateConstructor();

  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await _initDatabase();

    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
    CREATE TABLE $tableName (
      $columnId INTEGER PRIMARY KEY,
      $columnFirstName TEXT NOT NULL,
      $columnLastName TEXT NOT NULL
    )
    ''');
  }

  // HELPER METHODS
  Future<int> insert(Map<String, dynamic> row) async {
    Database db = await instance.database;

    return await db.insert(tableName, row);
  }

  Future<List<RegisterUser>> queryAllRows() async {
    Database db = await instance.database;
    var res = await db.query(tableName);
    List<RegisterUser> list = res.isNotEmpty
        ? res.map((e) => RegisterUser.fromMap(e)).toList()
        : null;
    return list;
  }

  Future<int> queryRowCount() async {
    Database db = await instance.database;

    return Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM $tableName'));
  }

  Future<int> update(Map<String, dynamic> row) async {
    Database db = await instance.database;

    int id = row[columnId];

    return await db
        .update(tableName, row, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<int> delete(int id) async {
    Database db = await instance.database;
    return await db.delete(tableName, where: '$columnId : ?', whereArgs: [id]);
  }
}
