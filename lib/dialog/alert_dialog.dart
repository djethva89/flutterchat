import 'package:flutter/material.dart';

class MyAlertDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Alert Dialog"),
        backgroundColor: Colors.deepPurple[600],
      ),
      body: new Container(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: ElevatedButton(
                      onPressed: () {
                        showAlertDialog(context);
                      },
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.amber[600])),
                      child: Text("         Show Alert!          ")),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: ElevatedButton(
                      onPressed: () {
                        _displayDialog(context);
                      },
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.green)),
                      child: Text("Show Alert With Input")),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

showAlertDialog(BuildContext context) {
  Widget okButton = TextButton(
      onPressed: () {
        Navigator.of(context).pop();
      },
      child: Text("OK"));

  AlertDialog alertDialog = AlertDialog(
    title: Text("My App Alert"),
    content: Text("This is an alert message."),
    actions: [
      okButton,
    ],
  );

  showDialog(
      context: context,
      builder: (BuildContext context) {
        return alertDialog;
      });
}

TextEditingController _textEditingController = TextEditingController();

_displayDialog(BuildContext context) async {
  _textEditingController.clear();
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("My Dialog"),
          content: TextField(
            controller: _textEditingController,
            decoration: InputDecoration(hintText: "Please enter your input..."),
          ),
          actions: <Widget>[
            new ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: new Text("Submit"))
          ],
        );
      });
}
