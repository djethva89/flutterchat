import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_components_demo_app/dialog/alert_dialog.dart';

import 'registration/register_list.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final List<String> componentsList = <String>[
    'Register User List',
    'CardView',
    'Alert Dialog',
    'Permission',
    'Image Picker',
    'Capture Photo'
  ];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Flutter Dashboard'),
        backgroundColor: Colors.red[400],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
                child: Text('Drawer Header',
                    style: TextStyle(color: Colors.white, fontSize: 24)),
                decoration: BoxDecoration(color: Colors.red[400])),
            ListTile(
              leading: Icon(Icons.home),
              title: Text('Home'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.logout),
              title: Text('Logout'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView.separated(
              itemBuilder: (BuildContext context, int index) {
                return InkWell(
                  child: Container(
                    height: 50,
                    color: Colors.teal[400],
                    child: Center(
                      child: Text(
                        componentsList[index],
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            letterSpacing: 2,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                  onTap: () {
                    switch (index) {
                      case 0:
                        {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => RegisterList(
                                      title: componentsList[index])));
                        }
                        break;
                      case 2:
                        {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MyAlertDialog()));
                        }
                        break;

                      default:
                        {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text('This Future Is Coming Soon!')));
                        }
                    }
                  },
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
              itemCount: componentsList.length),
        ),
      ),
    );
  }
}
