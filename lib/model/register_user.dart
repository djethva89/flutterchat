class RegisterUser {
  int _id;
  String first_name;
  String last_name;

  RegisterUser(this.first_name, this.last_name);

  RegisterUser.fromMap(dynamic obj) {
    this.first_name = obj['first_name'];
    this.last_name = obj['last_name'];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["first_name"] = first_name;
    map["last_name"] = last_name;
    return map;
  }
}
