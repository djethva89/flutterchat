import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_components_demo_app/database_util/database_helper.dart';

class RegisterForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Registration Form'),
      ),
      body: new RegisterWidgetClass(),
    );
  }
}

class RegisterWidgetClass extends StatefulWidget {
  @override
  _RegisterWidgetClassState createState() => _RegisterWidgetClassState();
}

class _RegisterWidgetClassState extends State<RegisterWidgetClass> {

  final dbHelper = DatabaseHelper.instance;

  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: firstNameController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    labelText: 'First Name',
                    labelStyle: TextStyle(fontSize: 14),
                    enabledBorder: OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 1),
                        borderRadius: BorderRadius.circular(2)),
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.blue, width: 1),
                        borderRadius: BorderRadius.circular(2))),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: lastNameController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    labelText: 'Last Name',
                    labelStyle: TextStyle(fontSize: 14),
                    enabledBorder: OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 1),
                        borderRadius: BorderRadius.circular(2)),
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.blue, width: 1),
                        borderRadius: BorderRadius.circular(2))),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                    onPressed: () {
                      _insert(firstNameController.text, lastNameController.text);
                    },
                    style: ElevatedButton.styleFrom(
                        primary: Colors.blue,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25.0))),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: const Text(
                        "Submit",
                        style: TextStyle(fontSize: 18.0),
                      ),
                    )),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _insert(String firstName, String lastName) async {
    Map<String, dynamic> row = {
      DatabaseHelper.columnFirstName: firstName,
      DatabaseHelper.columnLastName: lastName
    };

    final id = await dbHelper.insert(row);
    print('inserted row id: $id');
  }

}
