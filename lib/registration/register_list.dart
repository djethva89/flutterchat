import 'package:flutter/material.dart';
import 'package:flutter_components_demo_app/database_util/database_helper.dart';
import 'package:flutter_components_demo_app/model/register_user.dart';

import 'register_form.dart';

class RegisterList extends StatefulWidget {
  final String title;

  RegisterList({this.title});

  @override
  _RegisterListState createState() => _RegisterListState();
}

class _RegisterListState extends State<RegisterList> {
  final dbHelper = DatabaseHelper.instance;

  List<RegisterUser> items = List<RegisterUser>.empty(growable: true);

  @override
  void initState() {
    super.initState();

    dbHelper.queryAllRows().then((value) => {
          setState(() {
            value.forEach((element) {
              items.add(element);
            });
          })
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => RegisterForm()))
        },
        label: Text('Add'),
        icon: Icon(Icons.add),
        backgroundColor: Colors.amber,
      ),
      body: new Center(
        child: ListView.builder(
            itemCount: items.length,
            itemBuilder: (context, index) {
              final item = items[index];
              return new GestureDetector(
                onTap: () => {},
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: new Card(
                    color: Colors.teal[400],
                    shadowColor: Colors.grey,
                    child: new Column(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: new Padding(
                              padding: new EdgeInsets.all(10.0),
                              child: new Text(
                                "${item.first_name} ${item.last_name}",
                                style: TextStyle(
                                    fontSize: 22.0, color: Colors.white),
                                textAlign: TextAlign.start,
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }),
      ),
    );
  }
}
